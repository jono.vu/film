/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: `Mount Film`,
  },
  plugins: [
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: `6l90c4v1u1ow`,
        accessToken: `8VUXA3B3RKN6a2WaHNOm7qPCJb8hnnsFJzuZt4aVaCw`,
      },
    },
  ],
}
