import React from "react"

import { useStaticQuery, graphql } from "gatsby"

import _ from "lodash"

export const useImages = () => {
  const res = useStaticQuery(graphql`
    {
      allContentfulImage {
        edges {
          node {
            id
            title
            slug
            images {
              file {
                url
              }
            }
            dateDeveloped
            developed {
              title
            }
            filmType {
              title
            }
            camera {
              title
            }
          }
        }
      }
    }
  `)

  const rawImages = res.allContentfulImage.edges

  const image = rawImages.map(rawImage => {
    const img = rawImage.node
    const imgArray = img.images.map(imgRaw => {
      return imgRaw.file.url
    })

    return {
      id: img.id,
      title: img.title,
      slug: img.slug,
      images: imgArray,
      thumbnail: img.images[0].file.url,
      date: img.dateDeveloped,
      developed: img.developed.title,
      filmType: img.filmType.title,
      camera: img.camera.title,
    }
  })

  return image
}
