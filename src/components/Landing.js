import React from "react"

import Layout from "./layout/index"
import Wrapper from "./Wrapper"

const Landing = () => {
  return (
    <Layout>
      <Wrapper />
    </Layout>
  )
}

export default Landing
