import React from "react"

import styles from "./css/header.module.scss"

const Header = () => {
  return (
    <header className={styles.header}>
      <span className={styles.title}>Film by Mike</span>
    </header>
  )
}

export default Header
