import React, { useState } from "react"

import Image from "./image/index"

import { useImages } from "../utils/data"

import styles from "./css/wrapper.module.scss"

const Wrapper = props => {
  const images = useImages()

  const [grid, setGrid] = useState(true)

  const [focus, setFocus] = useState(0)

  return (
    <section className={grid ? styles.grid : styles.lightbox}>
      {images.map((image, i) => {
        return (
          <Image
            key={image.id}
            totalNumber={images.length}
            number={i}
            focus={focus}
            setFocus={e => setFocus(e)}
            image={image}
            grid={grid}
            setGrid={e => setGrid(e)}
          />
        )
      })}
    </section>
  )
}

export default Wrapper
