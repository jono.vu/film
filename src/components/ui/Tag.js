import React from "react"

import styles from "./css/tag.module.scss"

const Tag = props => {
  const children = props.children
  return <span className={styles.tag}>{props.children}</span>
}

export default Tag
