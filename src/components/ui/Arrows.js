import React from "react"

import Indicator from "./Indicator"

import styles from "./css/arrows.module.scss"

const Arrow = props => {
  const number = props.number
  const totalNumber = props.totalNumber
  const setFocus = props.setFocus

  const direction = props.direction

  const nextNumber = direction === "left" ? number - 1 : number + 1

  const isVisible = () => {
    if (nextNumber === -1) return false
    else if (nextNumber === totalNumber) return false
    else return true
  }

  return (
    <div className={!isVisible() && styles.disabled}>
      <wrapper className={styles.arrow}>
        <span
          onClick={() => setFocus(nextNumber)}
          className={direction === `left` ? styles.left : styles.right}
        >
          {props.children}
        </span>
      </wrapper>
    </div>
  )
}

const Arrows = props => {
  return (
    <wrapper className={styles.arrows}>
      <Arrow direction="left" {...props}>
        {`<< Left`}
      </Arrow>
      <Indicator {...props} />
      <Arrow direction="right" {...props}>
        {`Right >>`}
      </Arrow>
    </wrapper>
  )
}

export default Arrows
