import React from "react"

import styles from "./css/img.module.scss"

const Img = props => {
  const src = props.src
  const isFocused = props.isFocused
  const grid = props.grid

  return (
    <>
      {isFocused && (
        <img src={src} className={!grid ? styles.lightbox : styles.grid} />
      )}
    </>
  )
}

export default Img
