import React from "react"

import styles from "./css/indicator.module.scss"

const Indicator = props => {
  const number = props.number + 1
  const totalNumber = props.totalNumber

  return (
    <span
      className={styles.indicator}
    >{`Image ${number} of ${totalNumber}`}</span>
  )
}

export default Indicator
