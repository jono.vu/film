import React from "react"

import Img from "../ui/Img"

const Files = props => {
  const files = props.image.images

  return (
    <>
      {files.map(url => {
        return <Img src={url} {...props} />
      })}
    </>
  )
}

export default Files
