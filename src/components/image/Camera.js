import React from "react"

import Tag from "../ui/Tag"

const Camera = props => {
  const camera = props.image.camera
  return <Tag>on {camera}</Tag>
}

export default Camera
