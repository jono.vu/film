import React from "react"

import Tag from "../ui/Tag"

const FilmType = props => {
  const filmType = props.image.filmType
  return <Tag>{filmType}</Tag>
}

export default FilmType
