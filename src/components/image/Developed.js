import React from "react"

import Tag from "../ui/Tag"

import moment from "moment"

const Developed = props => {
  const developed = props.image.developed
  const date = props.image.date

  const formattedDate = moment(date).format(`Do MMM[-]YY`)

  return (
    <Tag>
      Developed {formattedDate}, <em>{developed}</em>
    </Tag>
  )
}

export default Developed
