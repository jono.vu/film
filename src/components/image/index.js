import React from "react"

import Ui from "./Ui"
import Files from "./Files"

const Surface = props => {
  const grid = props.grid
  const setGrid = props.setGrid
  const number = props.number

  const setFocus = props.setFocus

  const handleClick = () => {
    if (grid) {
      setFocus(number)
      setGrid(false)
    } else if (!grid) {
      setFocus(null)
      setGrid(true)
    }
  }

  return <div onClick={() => handleClick()}>{props.children}</div>
}

const Image = props => {
  const grid = props.grid
  const number = props.number

  const focus = props.focus
  const setFocus = props.setFocus

  const isFocused = () => {
    if (grid) return true
    else if (!grid && focus === number) return true
    else return false
  }

  return (
    <>
      {isFocused() && (
        <>
          {!grid && <Ui {...props} />}
          <Surface {...props}>
            <Files {...props} isFocused={isFocused()} />
          </Surface>
        </>
      )}
    </>
  )
}

export default Image
