import React from "react"

import Arrows from "../ui/Arrows"
import Indicator from "../ui/Indicator"
import Title from "./Title"
import FilmType from "./FilmType"
import Developed from "./Developed"
import Camera from "./Camera"

import styles from "./css/ui.module.scss"

const Ui = props => {
  return (
    <wrapper className={styles.ui}>
      <Arrows {...props} />
      <wrapper className={styles.info}>
        <Title {...props} />
        <wrapper className={styles.details}>
          <FilmType {...props} />
          <Developed {...props} />
          <Camera {...props} />
        </wrapper>
      </wrapper>
    </wrapper>
  )
}

export default Ui
