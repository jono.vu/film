import React from "react"

import styles from "./css/title.module.scss"

const Title = props => {
  const title = props.image.title
  return (
    <>
      <h4 className={styles.title}>{title}</h4>
    </>
  )
}

export default Title
